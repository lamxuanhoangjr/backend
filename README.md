# Backend

Viết bằng TypeScript

Test bằng Jest

## Các thư viện sử dụng

Xem thêm tại [đây](package.json)

## Cấu trúc dự án

+ `src/`: Thư mục chứa code dữ án
    + `config/`: Dùng để chứa cấu hình database
    + `controllers`: Chứa các code controllers
    + `routes`: Chứa các code routes
    + `tests`: Chứa các code test
+ `.env`: Chứa biến môi trường để cấu hình cho server
+ `jest.config.ts`: Chứa các cấu hình cho framework test JEST
+ `package.json`: Chứa các packages và là cấu hình dự án
+ `ts.config.json`: Chứa các cấu hình cho TypeScript

## Script của dự án

Xem thêm tại [đây](package.json)

Để chạy server trực tiếp
```sh
npm run start-now
```

Biên dịch TypeScript sang JavaScript và chạy server
```sh
npm run compile
npm run start
```

Chạy automation test
```sh
npm run jest
```

Chạy automation test với bao quát
```sh
npm run jest-coverage
```

## FAQ

Q: Các thư mục nào nên thêm hoặc thay đổi?
A: Các đồng chí vui lòng bỏ code vào thư mục `src` và ngoài ra không thêm bất
cứ thư mục nằm ngoài khác. Trừ khi các đồng chí biết mình đang làm gì. Nếu thêm
hãy ghi rõ trong Pull Request tại sao file đó lại có.
