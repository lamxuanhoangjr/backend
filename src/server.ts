// Lấy thư viện
import dotenv from "dotenv"
import cors from "cors"
import express from "express"

import RouterLopHoc from "./routes/lophoc"

// Setup các biến
dotenv.config()
const PORT: number = parseInt(process.env.API_PORT as string) || 3000
const app = express()

// Gom các routes
app.use(cors())
app.use("/api", RouterLopHoc)

// Khởi động server
app.listen(PORT, () => {
  console.log(`Server is running at http://localhost:${PORT}`)
})