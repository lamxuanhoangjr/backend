import { Client, QueryResult } from "pg"

import poolConfig from "../config/database"
import { LopHoc } from "../models/lophoc"

class ServiceLopHoc implements LopHoc {
  lophoc_id!: number
  lophoc_name!: string

  async insert(id: number, name: string): Promise<boolean>
  {
    const client: Client = new Client(poolConfig)

    try {
      // Connect database
      await client.connect()

      // Execute SQL
      await client.query({
        text: "CALL insert_lophoc($1, $2)",
        values: [id, name],
      })

    } catch (error) {
      // Return undefined
      console.log(error)
      return false
    } finally {
      // Close database
      await client.end()
    }

    return true
  }

  async getAll(limit: number, offset: number):
    Promise<LopHoc[] | undefined>
  {
    const client: Client = new Client(poolConfig)
    let allLopHoc: LopHoc[] = []

    try {
      // Connect database
      await client.connect()

      // Execute SQL
      const result: QueryResult = await client.query({
        text: "SELECT * FROM getall_lophoc_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to LopHoc
      allLopHoc = result.rows.map((row: LopHoc) => ({
        lophoc_id: row.lophoc_id,
        lophoc_name: row.lophoc_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    } finally {
      // Close database
      await client.end()
    }

    return allLopHoc
  }
}

export default ServiceLopHoc