import express from "express"
import ControllerLopHoc from "../controllers/lophoc"

const router = express.Router()

router.get("/lophoc", ControllerLopHoc)

export default router