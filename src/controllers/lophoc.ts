import { Request, Response } from 'express'
import ServiceLopHoc from '../services/lophoc'

async function ControllerLopHoc(req: Request, res: Response): Promise<void> {
  try {
    const { limit, offset } = req.query

    if (!limit || !offset) {
      res.status(400).json({ message: 'Bad Request: Both limit and offset are required.' })
      return
    }

    const service = new ServiceLopHoc()
    const result = await service.getAll(
      parseInt(limit as string), parseInt(offset as string)
    )

    if (result) {
      res.json(result)
    } else {
      res.status(500).json({ message: 'Internal Server Error: Unable to fetch data.' })
    }
  } catch (error) {
    console.error('Error in ControllerLopHoc:', error)
    res.status(500).json({ message: 'Internal Server Error' })
  }
}

export default ControllerLopHoc