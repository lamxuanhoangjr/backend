import ServiceLopHoc from "../../services/lophoc"

test("trả về hết bảng lớp học dựa trên limit và page", async () => {
  let sv: ServiceLopHoc = new ServiceLopHoc()
  const result = await sv.getAll(10, 0)
  expect(result).not.toBe(undefined)
})