import { PoolConfig } from "pg"

interface DatabaseConfig extends PoolConfig {
  user?: string
  password?: string
  host?: string
  port?: number
  database?: string
}

const poolConfig: DatabaseConfig = {
  user: process.env.DATABASE_USER || "postgres",
  password: process.env.DATABASE_PASSWORD || "postgres",
  host: process.env.DATABASE_HOST || "localhost",
  port: parseInt(process.env.DATABASE_PORT as string) || 5432,
  database: process.env.DATABASE_DATABASE || "thi",
}

export default poolConfig